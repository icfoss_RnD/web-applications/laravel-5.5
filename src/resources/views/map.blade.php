@extends('layouts.master')
@section('content')
<div class="container" id="test">
 <div class="row">
  <div class="col-md-12">
  <h5 class="text-center">For further enquiries, contact <a class="nav-link" href="tel:9745004213">9745004213</a></h5>
    <form autocomplete="off" method="POST" accept-charset="UTF-8" id="map_form" class="well form-horizontal">
    {{csrf_field()}}
    <div class="col-md-12" >
        <div class="row">
            <span class=" hidden-xs">
                <img src="{{secure_asset('images/topbar_logo.png')}}" style="" alt="">
            </span>
            <div class="pull-right">
                <div class="dropdown user_profile">
                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                {{$user}}
                <span class="caret"></span></button>
                <ul class="dropdown-menu dropdown-menu-right">
                <li><a href="{{URL::to('/logout')}}">Logout</a></li>
                </ul>
                </div>
            </div>
        </div>
    </div>

    @if(!isset($officess))
    <div class="form-group">
      <div class="col-md-12 inputGroupContainer">
      <div style="height:300px;" class="col-xs-12 well" id="map"></div>
      <input type="hidden" id="gps_lat" name="gps_lat" placeholder="latitude" readonly />
      <input type="hidden" id="gps_lng" name="gps_lng" placeholder="longitude" readonly />
      <input type="hidden" id="gps_addr" name="gps_addr" placeholder="Located Address" readonly />
      <ul id="status" class="progressing" style="display:none">
      </ul>
    </div>
    <div class="col-md-4 col-md-offset-4">
        <button class="btn btn-block btn-primary" id="locate" type="button" onclick="locateMe()">Locate <i class="fa fa-map-marker"></i></button>
    </div>
    </div>
    @else
    {{method_field('PUT')}}
    @endif

    <!-- section div -->
@php 
$districts=['THIRUVANANTHAPURAM','KOLLAM','ALAPPUZHA','PATHANAMTHITTA','KOTTAYAM','IDUKKI','ERNAKULAM','THRISSUR','PALAKKAD','MALAPPURAM','KOZHIKODE','WAYANAD','KANNUR','KASARAGOD'];
$localbodies=['CORPORATION','MUNICIPALITY','PANCHAYATH'];
$selected_district=$selected_localbody=$selected_department='';
if(isset($officess)){
    $selected_district=$officess[0]->district;
    $selected_localbody=$officess[0]->localbody_type;
    $selected_department=$officess[0]->dept_id;
}
@endphp 
    <!-- section -->
    <div class="form-section">
    <div class="col-md-12 text-center">
      <h4><strong>OFFICE INFORMATION</strong></h4>
    </div>
    <!-- section ends -->

    @if(!isset($officess))
    <!-- section -->
    <div class="form-section" style="display:none">
        <div class="form-group">
         <label for="gps_addr" class="col-md-4 control-label padding-left"> Located Address</label>
            <div class="col-md-6 inputGroupContainer">
                <div class="">
                    <textarea id="gps_addr" placeholder="Located Address" class="form-control" readonly></textarea>
                </div>
            </div>
        </div>
    </div>
    <!-- section ends -->

    <!-- section -->
    <div class="form-section">
        <div class="form-group">
         <label for="gps_accy" class="col-md-4 control-label padding-left"> GPS Accuracy</label>
            <div class="col-md-6 inputGroupContainer">
                <div class="">
                    <input type="text" class="form-control" id="gps_accy" name="gps_accy" placeholder="GPS Accuracy" readonly />
                </div>
            </div>
        </div>
    </div>
    <!-- section ends -->

    <div class="form-group">
        <label for="dept_name" class="col-md-4 control-label padding-left">Department Name</label>
        <div class="col-md-6 inputGroupContainer">
            <select class="form-control" id="dept_name" name="dept_name">
            <option value="" selected="selected">Select Department</option>
            @foreach ($departments as $department) 
            <option value="{{ $department->id }}">{{ $department->department_name }}</option>
            @endforeach
            </select>
        </div>
    </div>
    </div>
    @endif

    <!-- section -->
    <div class="form-section" id="custom_office_name_sec">
        <div class="form-group">
         <label for="custom_office_name" class="col-md-4 control-label padding-left" required> Office name</label>
            <div class="col-md-6 inputGroupContainer">
                <div class="">
                    <input class="form-control" placeholder=" " maxlength="100" name="custom_office_name" type="text" value="{{$officess[0]->office_name or ''}}" id="custom_office_name" />
                </div>
            </div>
        </div>
    </div>
    <!-- section ends -->

    <!-- section -->
    <div class="form-section">
        <div class="form-group">
         <label for="place" class="col-md-4 control-label padding-left"> Place</label>
            <div class="col-md-6 inputGroupContainer">
                <div class="">
                    <input class="form-control" placeholder=" " maxlength="50" name="place" type="text" id="place"  value="{{$officess[0]->place or ''}}" required />
                </div>
            </div>
        </div>
    </div>
    <!-- section ends -->

    <!-- section -->
    <div class="form-section">
        <div class="form-group">
         <label for="buildno" class="col-md-4 control-label padding-left"> Building Number</label>
            <div class="col-md-6 inputGroupContainer">
                <div class="">
                    <input class="form-control" placeholder=" " maxlength="30" name="buildno" type="text" value="{{$officess[0]->build_num or ''}}" id="buildno" />
                </div>
            </div>
        </div>
    </div>
    <!-- section ends -->

    <!-- section -->
    <div class="form-section">
        <div class="form-group">
         <label for="streetname" class="col-md-4 control-label padding-left"> Street Name</label>
            <div class="col-md-6 inputGroupContainer">
                <div class="">
                    <input class="form-control" placeholder=" " maxlength="50" name="streetname" type="text" id="streetname" value="{{$officess[0]->street_name or ''}}" />
                </div>
            </div>
        </div>
    </div>
    <!-- section ends -->

    <!-- section -->
    <div class="form-section">
        <div class="form-group">
         <label for="post" class="col-md-4 control-label padding-left"> Post Code</label>
            <div class="col-md-6 inputGroupContainer">
                <div class="">
                        <input class="form-control" placeholder=" " maxlength="10" name="post" type="text" id="post"  value="{{$officess[0]->post_code or ''}}"/>
                </div>
            </div>
        </div>
    </div>
    <!-- section ends -->

    <!-- section -->
    <div class="form-section">
        <div class="form-group">
         <label for="city" class="col-md-4 control-label padding-left"> City</label>
            <div class="col-md-6 inputGroupContainer">
                <div class="">
                        <input class="form-control" placeholder=" " maxlength="40" name="city" type="text" id="city"  value="{{$officess[0]->city or ''}}"/>
                </div>
            </div>
        </div>
    </div>
    <!-- section ends -->

    <!-- section -->
    <div class="form-section">
        <div class="form-group">
         <label for="district" class="col-md-4 control-label padding-left"> District</label>
            <div class="col-md-6 inputGroupContainer">
                <div class="">
                <select class="form-control" id="district" name="district">
                    <option value="" selected="selected">Select Your District</option>
                    @foreach ($districts as $district)
                    <option value="{{ $district}}" {{ ( $district == $selected_district ) ? 'selected' : '' }}> {{ $district }} </option>
                    @endforeach 
                </select>
                </div>
            </div>
        </div>
    </div>
    <!-- section ends -->

    <!-- section -->
    <div class="form-section">
        <div class="form-group">
         <label for="lb_type" class="col-md-4 control-label padding-left"> Local Body Type</label>
            <div class="col-md-6 inputGroupContainer">
                <div class="">
                <select class="form-control" id="lb_type" name="lb_type">
                    <option value="" selected="selected">Select Your Local Body Type</option>
                    @foreach ($localbodies as $localbody)
                    <option value="{{ $localbody}}" {{ ( $localbody == $selected_localbody ) ? 'selected' : '' }}> {{ $localbody }} </option>
                    @endforeach
                </select>
                </div>
            </div>
        </div>
    </div>
    <!-- section ends -->

    <!-- section -->
    <div class="form-section">
        <div class="form-group">
         <label for="working_hours" class="col-md-4 control-label padding-left">Working Hours</label>
            <div class="col-md-6 inputGroupContainer">
                <div class="">
                    <input class="form-control" placeholder="eg: (10:00AM-06:00PM)" maxlength="20" name="working_hours" type="text" id="working_hours" value="{{$officess[0]->working_hours or ''}}"/>
                </div>
            </div>
        </div>
    </div>
    <!-- section ends -->

    <!-- section -->
    <div class="form-section">
        <div class="form-group">
         <label for="phonenum" class="col-md-4 control-label padding-left">Phone Number</label>
            <div class="col-md-6 inputGroupContainer">
                <div class="">
                    <input class="form-control" placeholder="eg: (0471-2700014)" maxlength="20" name="phonenum" type="text" id="phonenum" value="{{$officess[0]->phone_number or ''}}"/>
                </div>
            </div>
        </div>
    </div>
    <!-- section ends -->

    <!-- section -->
    <div class="form-section">
        <div class="form-group">
         <label for="mobilenum" class="col-md-4 control-label padding-left">Mobile Number</label>
            <div class="col-md-6 inputGroupContainer">
                <div class="">
                    <input class="form-control" placeholder="eg: (9876543210)" maxlength="12" name="mobilenum" type="text" id="mobilenum" value="{{$officess[0]->mobile_number or ''}}"/>
                </div>
            </div>
        </div>
    </div>
    <!-- section ends -->

    <!-- section -->
    <div class="form-section">
        <div class="form-group">
         <label for="office_mail" class="col-md-4 control-label padding-left">OFFICE Email</label>
            <div class="col-md-6 inputGroupContainer">
                <div class="">
                    <input class="form-control" placeholder="" maxlength="40" name="office_mail" type="email" id="office_mail" value="{{$officess[0]->office_mail or ''}}"/>
                </div>
            </div>
        </div>
    </div>
    <!-- section ends -->

    <!-- section -->
    <div class="form-section">
        <div class="form-group">
         <label for="website" class="col-md-4 control-label padding-left">OFFICE Website Link</label>
            <div class="col-md-6 inputGroupContainer">
                <div class="">
                    <input class="form-control" placeholder="eg: (https://icfoss.in)" maxlength="40" name="website" type="text" id="website" value="{{$officess[0]->website or ''}}"/>
                </div>
            </div>
        </div>
    </div>
    <!-- section ends -->

    <!-- section -->
    <div class="form-section">
        <div class="form-group">
         <label for="remarks" class="col-md-4 control-label padding-left">Extra information</label>
            <div class="col-md-6 inputGroupContainer">
                <div class="">
                    <textarea class="form-control" maxlength="100" name="extra_info" id="extra_info">{{$officess[0]->extra_info or ''}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <!-- section ends -->

    <!-- section -->
    <div class="form-section">
        <div class="form-group">
         <label for="remarks" class="col-md-4 control-label padding-left">Remarks</label>
            <div class="col-md-6 inputGroupContainer">
                <div class="">
                    <textarea class="form-control" maxlength="100" name="remarks" id="remarks">{{$officess[0]->remarks or ''}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <!-- section ends -->

    <div class="form-group">
        <div class="col-md-offset-4 col-md-6 inputGroupContainer alert-danger" style="display:none"></div>
    </div>

    <!-- Button -->
    <div class="form-group">
        <div class="col-md-12 "style="text-align: center;">
            <button id="submit" type="submit" class="btn btn-primary">Save<span class="glyphicon glyphicon-send"></span></button>
        </div>
    </div>
    </form>
  </div>
 </div>
</div>

<div id="messageModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <p id="modal_message">Thank you</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="loadingModal" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body text-center noselect">
	<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><br>Loading..
      </div>
    </div>
  </div>
</div>

@push('bodyscripts')
<script type='text/javascript'>

$(document).ajaxStart(function(){
    $("#loadingModal").modal("show");
});
$(document).ajaxComplete(function(){
    $("#loadingModal").modal("hide");
});

function formatData(json_data){ //format the attributes of each features for displaying in popup table format
    var html='<table>';
    for(key in json_data){
        html+='<tr><b>'+key+': </b>'+json_data[key]+'<br/></tr>';
    }
    html+='</table>';
    return html;
}

function getMapData(bbox){
    $.ajax({
        type: "GET",
        url: "{{URL::to('/map_data')}}",
        dataType: "json",
        data: {bbox: bbox},
        success: function(result){
            status = result.status;
            if(status == 'true'){
                var greenIcon = new L.Icon({
                iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                });
                data = result.data;
                for(index=0;index<data.length;index++){
                    html = formatData(data[index]);
                    var location = new L.LatLng(data[index].gps_lat,data[index].gps_lng);
                    var marker = new L.Marker(location,{icon: greenIcon});
                    marker.bindPopup("<div style='text-align: center; margin-left: auto; margin-right: auto;'>"+html+"</div>", {maxWidth: '400'})
                    .addTo(map);
                }
            }
            else{
                $('#modal_message').html('<span style="color:red">Loading map data has failed</span>');
                $('#messageModal').modal('show');
            }
        }
    });
}
</script>
@if(!isset($officess))
<script type='text/javascript'>
    var theMarker,theCircle = {};
    var map = L.map('map', {
        center: [10.833, 76.27],
        zoom: 7,
    });
    
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    function onAccuratePositionProgress (e) {
        var message = 'Progressing … (Accuracy: ' + e.accuracy + ')';
    }

    function onAccuratePositionFound (e) {

        var message = 'Most accurate position found (Accuracy: ' + e.accuracy + ')';

        $('#progress').remove();
        $('#locate').html('Locate <i class="fa fa-map-marker"></i>');
        $('#locate').removeAttr('disabled');

        //store values to form fields
        $('#gps_lat').val(e.latitude);
        $('#gps_lng').val(e.longitude);
        $('#gps_accy').val(e.accuracy);
        //getAddressFromNominatim(e.latitude,e.longitude);
        //reValidate given field http://bootstrapvalidator.votintsev.ru/api/#revalidate-field
        $('#map_form').data('bootstrapValidator').revalidateField('gps_accy');
        var radius = e.accuracy;
        theMarker = L.marker(e.latlng).addTo(map)
        .bindPopup("You are within " + radius + " meters from this point").openPopup();
        theCircle = L.circle(e.latlng, radius).addTo(map);

        map.setView([e.latitude,e.longitude], 16);
        var bbox = map.getBounds().toBBoxString();
        //get map data
        getMapData(bbox);
    }

    function onAccuratePositionError (e) {
        $('#modal_message').html('<span style="color:red">'+e.message+'</span>');
        $('#messageModal').modal('show');
        $('#progress').remove();
        $('#locate').html('Locate <i class="fa fa-map-marker"></i>');
        $('#locate').removeAttr('disabled');
    }

    map.on('accuratepositionprogress', onAccuratePositionProgress);
    map.on('accuratepositionfound', onAccuratePositionFound);
    map.on('accuratepositionerror', onAccuratePositionError);

    function locateMe(){
        //removing existing point from map on second locate
        if (theMarker != undefined)
            map.removeLayer(theMarker);
        if (theCircle != undefined)
            map.removeLayer(theCircle);

        map.findAccuratePosition({
            maxWait: 30000, // defaults to 30000
            desiredAccuracy: 10, // defaults to 10,
        });

        $('#locate').html('Locating <i class="fa fa-spinner fa-spin"></i>');
        $('#locate').attr('disabled','disabled');
    }

    function addStatus(message, className) {
        var ul = document.getElementById('status'),
        li = document.createElement('li');
        li.appendChild(document.createTextNode(message));
        ul.className = className;
        ul.appendChild(li);
    }

    function getAddressFromNominatim(lat,lng){
        $.ajax({
            type: "GET",
            url: "https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat="+lat+"&lon="+lng,
            dataType: "json",
            success: function(result){
                $('#gps_addr').val(result.display_name);
            }
        });
    }
</script>
@endif

<script type='text/javascript'>
$(document).ready(function() {
$('#map_form')[0].reset();
$('#map_form').bootstrapValidator({
    // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        custom_office_name: {
            validators: {
                 stringLength: {
                    min: 1,
                    max: 100,
                },
                notEmpty: {
                    message: 'Please enter Office name'
                }
            }
        },
        place: {
            validators: {
                 stringLength: {
                    min: 1,
                    max: 50,
                },
                notEmpty: {
                    message: 'Please enter Place name'
                }
            }
        },
        gps_accy: {
            validators: {
                lessThan: {
                    value: 20,
                    inclusive: true,
                    message: 'GPS accuracy has to be less than 20 meters'
                },
                notEmpty: {
                    message: 'Please capture location by pressing locate button'
                }
            }
        }
    }
    })
    .on('success.form.bv', function(e) {
        $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
        $('#map_form').data('bootstrapValidator').resetForm();
        // Prevent form submission
        e.preventDefault();
        // Get the form instance
        var $form = $(e.target);
        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');
        // Use Ajax to submit form data
        $.post($form.attr('action'), $form.serialize(), function(result) {
            //
            @if(!isset($officess))
            var bbox = map.getBounds().toBBoxString();
            //get map data
            getMapData(bbox);
            if(result){
                map.removeLayer(theMarker);
                map.removeLayer(theCircle);
                $('#modal_message').html('<span style="color:green">Thank you</span>');
                $('#messageModal').modal('show');
                $form[0].reset();
                $('#messageModal').on('hidden.bs.modal', function (e) {
                if(result.hasOwnProperty('redirect')){
                    window.location.href = result.redirect;
                }
                });
            }
            else{
                $('#modal_message').html('<span style="color:red">Updating map has failed</span>');
                $('#messageModal').modal('show');
            }
            @else
            if(result){
                $('#modal_message').html('<span style="color:green">Thank you</span>');
                $('#messageModal').modal('show');
                $form[0].reset();
                $('#messageModal').on('hidden.bs.modal', function (e) {
                if(result.hasOwnProperty('redirect')){
                    window.location.href = result.redirect;
                }
                });
            }
            else{
                $('#modal_message').html('<span style="color:red">Updating map has failed</span>');
                $('#messageModal').modal('show');
            }
            @endif
        }, 'json');
    });
});
</script>
@endpush
@endsection