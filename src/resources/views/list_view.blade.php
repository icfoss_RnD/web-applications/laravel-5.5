@extends('layouts.master')
@push('headscripts')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('content')
<div class="container" id="test">
 <div class="row">
  <div class="col-md-12">
  <h5 class="text-center">For further enquiries, contact <a class="nav-link" href="tel:9745004213">9745004213</a></h5>
    <form autocomplete="off" method="POST" accept-charset="UTF-8" id="map_form" class="well form-horizontal">
    {{csrf_field()}}
    <div class="col-md-12" >
        <div class="row">
            <span class=" hidden-xs">
                <img src="{{secure_asset('images/topbar_logo.png')}}" style="" alt="">
            </span>
            <div class="pull-right">
                <div class="dropdown user_profile">
                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                {{$user}}
                <span class="caret"></span></button>
                <ul class="dropdown-menu dropdown-menu-right">
                <li><a href="{{URL::to('/logout')}}">Logout</a></li>
                </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-md-offset-4">
    <a href="{{url('/map')}}" class="btn btn-block btn-primary" id="create_button">Add new <i class="fa fa-map-marker"></i></a><br/>
    </div>
    <div class="table-responsive" style="width: 100%">
        <table id="officess" class="table table-hover table-bordered display row-border hover order-column cell-border dt-responsive display " style="width:100%"  >
            <thead>
                <tr>
                    <th>Sl.No</th>
                    <th >Office Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($offices as $i=>$office) 
             <tr>
             <td>{{ $i+1 }}</td>
                <td>{{$office->office_name}}</td>
                <td><a href="{{url('/map')}}/{{$office->id}}" class="btn btn-xs btn-primary" style=" margin-right:10px;" id="edit_office">Edit</a>
                <button type="button" class="btn btn-xs btn-danger delete_office" id="" value="{{$office->id}}">Delete</button></td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @push('bodyscripts')
<script type='text/javascript'>
$(document).ready(function() {
$('.delete_office').on('click',function(){
var delete_status=confirm("Are you sure want to delete this record");
if(delete_status== true){
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
    var id=$('.delete_office').val();
    $.ajax({
             url: APP_URL+"/map/"+id,
             type: "DELETE",
             dataType: "json",
             success: function(result){
                location.reload(); 
            }
            });
}
});
});
</script>
@endpush
@endsection
