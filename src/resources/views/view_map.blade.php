@extends('layouts.error')
@push('headscripts')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.3/leaflet.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.4.1/MarkerCluster.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.4.1/MarkerCluster.Default.css" />
<script>
var APP_URL = '{{URL::to("/")}}';
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.3/leaflet.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.4.1/leaflet.markercluster.js"></script>
@endpush
@section('content')
<div class="container">
    <h3 style="text-decoration: none !important;color:black !important" class="text-center">
    <a class="btn btn-primary" href="{{URL::to('/view_stats')}}">View Stats</a>
    <a class="btn btn-danger" href="{{URL::to('/view_table')}}">View Table</a>
    </h3>
    <div class="row">
    <div style="height:500px;" class="col-xs-12 well" id="map"></div>
    </div>
</div>

<div id="messageModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <p id="modal_message">Thank you</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

@endsection
@push('bodyscripts')
<script>
    var map = L.map('map', {
        center: [10.833, 76.27],
        zoom: 7,
    });
    
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    function formatData(json_data){ //format the attributes of each features for displaying in popup table format
      var html='<table>';
      for(key in json_data){
        html+='<tr><b>'+key+': </b>'+json_data[key]+'<br/></tr>';
      }
    html+='</table>';
    return html;
}

function getMapData(bbox){
    $.ajax({
        type: "GET",
        url: "{{URL::to('/map_data')}}",
        dataType: "json",
        data: {bbox: bbox},
        success: function(result){
            status = result.status;
            if(status == 'true'){
                var greenIcon = new L.Icon({
                iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
                iconSize: [17, 26],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                });
                data = result.data;
                var markerClusters = L.markerClusterGroup();
                for(index=0;index<data.length;index++){
                    html = formatData(data[index]);
                    var location = new L.LatLng(data[index].gps_lat,data[index].gps_lng);
                    var marker = new L.Marker(location,{icon: greenIcon});
                    marker.bindPopup("<div style='text-align: center; margin-left: auto; margin-right: auto;'>"+html+"</div>", {maxWidth: '400'});
                    markerClusters.addLayer( marker );
                }
                map.addLayer( markerClusters );
            }
            else{
                $('#modal_message').html('<span style="color:red">Loading map data has failed</span>');
                $('#messageModal').modal('show');
            }
        }
    });
}

var bbox = map.getBounds().toBBoxString();
//get map data
getMapData(bbox);
</script>
@endpush