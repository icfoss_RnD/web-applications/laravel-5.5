@extends('layouts.master')
@section('content')
<div class="container">
<div class="row" style="margin-top:60px;">
<div class=" col-md-offset-1 col-md-10">
    <form method="POST" accept-charset="UTF-8" id="map_form" class="well form-horizontal" action="{{ route('login') }}">
    {{csrf_field()}}

    <div class="form-group text-center">
    <h4>Government Buildings Mapping</h4>
    </div>

    <div class="form-group">
     <div class="col-md-offset-4 col-md-6 inputGroupContainer alert-danger" style="display:none">
     </div>
    </div>

    <div class="panel-body">
                    <!-- <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }} -->

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>
                            </div>
                        </div>
                    </form>


    <!-- Button -->
    <div class="form-group">
    <label class="col-md-4 control-label">Login using</label>
    <div class="col-md-8">
        <a class="col-sm-2 btn-xs-block btn btn-default" href="{{ url('/login/gitlab') }}">
         <i class="fa fa-gitlab"></i> Gitlab
        </a>
        <a class="col-sm-2 btn-xs-block btn btn-default" href="{{ url('/login/github') }}">
         <i class="fa fa-github"></i> Github
        </a>
        <a class="col-sm-2 btn-xs-block btn btn-default" href="{{ url('/login/google') }}">
         <i class="fa fa-google"></i> Google
        </a>
        <a class="col-sm-2 btn-xs-block btn btn-default" href="{{ url('/login/facebook') }}">
         <i class="fa fa-facebook"></i> Facebook
        </a>
        <!-- <a class="col-sm-2 btn-xs-block btn btn-default" href="{{ route('login') }}">
         <i ></i> Register And Login
        </a> -->
    </div>
    </div>

    
  </div>
</div>
</div>
@endsection