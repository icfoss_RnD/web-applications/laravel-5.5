@extends('layouts.error')
@section('content')
    <div class="text-center margin-top-bottom-10per">
	<h2 style="color: #31708f;">
	    ERROR {{ $exception->getStatusCode() }}
	</h2>
	<h4 style="color:red">{{ $exception->getMessage() }}</h4>
    </div>
@endsection
