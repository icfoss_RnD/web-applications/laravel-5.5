@extends('layouts.master')
@section('content')
<div class="container">
<div class="row" style="margin-top:60px;">
   <div class=" col-md-offset-1 col-md-10">
          
         <span class="site_logo visible-xs">
          <img src="{{secure_asset('css/png_logo(1).png')}}"  style="width:35%"alt="">
          </span>

            <span class="site_logo hidden-xs">
            <img src="{{secure_asset('css/png_logo(1).png')}}"  style="width:15%"alt="">
            </span>

         <div class="panel-body">
            <form autocomplete="off" method="POST" accept-charset="UTF-8" id="login_form" class="well form-horizontal" action="{{ route('login') }}">
            {{csrf_field()}}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
               <!-- <label for="email" class="col-md-4 control-label">E-Mail Address</label> -->
               <div class="col-md-12 ">
                  <input id="email" type="email" class="form-control"  placeholder="Email " name="email" value="{{ old('email') }}" required autofocus>
                  @if ($errors->has('email'))
                  <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  @endif
               </div>
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
               <div class="col-md-12">
                  <input id="password" type="password" placeholder="Password " class="form-control" name="password" required>
                  @if ($errors->has('password'))
                  <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                  </span>
                  @endif
               </div>
            </div>
            <div class="form-group" id="login_reg">
               <div class="col-md-12 ">
                  <button type="submit" class="btn btn-primary">
                  Login
                  </button>
                  <a class="btn btn-default" href="{{ route('register') }}">Register</a>
               </div>
            </div>
      </div>          
      
      <!-- Button -->
        <div class="form-group text-center">
            <label class="col-md-12">OR <br/><br/> Login using</label>
            <div class="col-md-12">
                <a class="col-sm-2 btn-xs-block btn btn-default float-none" href="{{ url('/login/gitlab') }}">
                    <i class="fa fa-gitlab"></i> Gitlab
                </a>
                <a class="col-sm-2 btn-xs-block btn btn-default float-none" href="{{ url('/login/github') }}">
                    <i class="fa fa-github"></i> Github
                </a>
                <a class="col-sm-2 btn-xs-block btn btn-default float-none" href="{{ url('/login/google') }}">
                    <i class="fa fa-google"></i> Google
                </a>
                <a class="col-sm-2 btn-xs-block btn btn-default float-none" href="{{ url('/login/facebook') }}">
                    <i class="fa fa-facebook"></i> Facebook
                </a>
            </div>
      </form>
      </div>
   </div>
</div>
</div>
@endsection
