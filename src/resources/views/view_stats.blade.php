@extends('layouts.error')
@push('headscripts')
<style>
.footer {
display: none;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap.min.css" />
<script>
var APP_URL = '{{URL::to("/")}}';
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/dataTables.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/dataTables.bootstrap.min.js"></script>
@endpush
@section('content')
<div class="container" style="position: relative; height:60vh; width:80vw">
<h3 style="text-decoration: none !important;color:black !important" class="text-center">
<!-- <a class="btn btn-success" href="{{URL::to('/view_map')}}">View Map</a> -->
<b><a class="btn btn-danger" href="{{URL::to('/view_table')}}">View district wise list of offices mapped</a></b>
</h3>
<h4 class="text-center">Total number of government offices mapped (<span class="text-info">excluding Akshayas</span>): <span id="count"></span></h4>
<h6 class="text-center">N.B.: The count may not match with estimation as there might be duplicate entries.</h6>
<canvas id="barChart"></canvas><br/>
<!-- <canvas id="pieChart"></canvas> -->
<div class="row well">
        <div class="col-md-12">
        <h4 class="text-center"><b>Map My Office Department-wise statistics</b></h4>
        <table id="example" class="table table-hover table-bordered display row-border hover order-column" style="width:100%">
            <thead>
                <tr>
                    <th>Department</th>
                    <th>Current</th>
                    <th>Target</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
        </div>
    </div>
</div>

@endsection
@push('bodyscripts')
<script src="{{asset('js/department_target.js')}}"></script>
<script>

function random255(){
    return Math.floor(Math.random() * 255) + 20;
}
var labels = [
    "Thiruvananthapuram",
    "Kollam",
    "Pathanamthitta",
    "Alappuzha",
    "Kottayam",
    "Idukki",
    "Ernakulam",
    "Thrissur",
    "Palakkad",
    "Malappuram",
    "Kozhikode",
    "Wayanad",
    "Kannur",
    "Kasaragod",
];
var data = [];

$.ajax({
    type: 'GET',
    dataType: 'json',
    url: APP_URL+'/districtwise_count',
    success: function(result){
        data = result;
        // renderPieChart(data,labels);
        renderBarChart(data,labels);
        var sum = data.reduce(function(a, b) { return a + b; }, 0);
        $('#count').html(sum);
    }
});

$.ajax({
    type: 'GET',
    dataType: 'json',
    url: APP_URL+'/deptwise_count',
    success: function(result){
        data = result;
        for(var i=0,j=0;i<data.length;i++){
            var html = '<tr>';
            html += `<td><a href="{{URL::to('/view_table')}}/?dept=`+encodeURIComponent(data[i].department_name)+`">`+data[i].department_name+`</td>`;
            html += '<td>'+data[i].count+'</td>';
            if(target_jsonarr[i].department==data[i].department_name){
                html+='<td>'+target_jsonarr[i].target+'</td>';
            }
            else{
                html+='<td>NA</td>';
            }
            html += '</tr>';
            $('#example tbody').append(html);
        }
        $('#example').DataTable();
    }
});

var color = [],borderColor = [],backgroundColor = [];
for(i=0;i<14;i++)
    color.push(random255()+','+random255()+','+random255());
for(i=0;i<color.length;i++){
    borderColor.push('rgba('+color[i]+',1)');
    backgroundColor.push('rgba('+color[i]+',0.5)');
}

function renderPieChart(data, labels) {
    var pie = document.getElementById("pieChart").getContext('2d');
    var pieChart = new Chart(pie, {
        type: 'pie',
        data: {
            labels: labels,
            datasets: [
                {
                    data: data,
                    borderColor: borderColor,
                    backgroundColor: backgroundColor,
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            title: {
                display: true,
                text: "Map My Office District-wise statistics",
                fontSize: 16
            }
        }
    });
}

function renderBarChart(data, labels) {
    var targetData = [3124, 2459, 1731, 2174, 2322, 1567, 2961, 2889, 2645, 3106, 2865, 1020, 2817, 1412];
    var barChartData = {
    labels: labels,
    datasets: [{
        label: "Current",
        backgroundColor: 'rgba(255, 99, 132, 0.5)',
        borderWidth: 1,
        data: data,
    }, {
        label: "Target",
        backgroundColor: 'rgba(255, 206, 86, 1)',
        borderWidth: 5,
        data: targetData,
        fill: false,
        type: 'line',
    }]
    };

    var options = {
        responsive: true,
        maintainAspectRatio: false,
        title: {
            display: true,
            text: "Map My Office District-wise target estimate",
            fontSize: 16
        },
    };

    var ctx = document.getElementById("barChart").getContext("2d");
    var barChart = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: options
    });
}
</script>
@endpush