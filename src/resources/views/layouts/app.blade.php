<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="author" content="ICFOSS">
        <meta name="description" content="Mapping Project">
        <meta name="keywords" content="ICFOSS">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Map My Office @yield('title')</title>
        <!-- style sheets -->
        <link href="{{secure_asset('/css/style.css')}}" rel="stylesheet" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.3/leaflet.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://rawgit.com/guillaumepotier/Parsley.js/2.2.0-rc4/src/parsley.css" />
        @stack('headstyles')

        <!-- scripts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.min.js"></script>
        <script src="https://rawgit.com/guillaumepotier/Parsley.js/2.2.0-rc4/dist/parsley.js"></script>
        <script src="https://scottoffen.github.io/jquery.toaster/jquery.toaster.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.3/leaflet-src.js"></script>
        <script src="{{secure_asset('/js/Leaflet.AccuratePosition.js')}}"></script>
        <script>
            var APP_URL = '{{URL::to("/")}}';
        </script>
	<style>
	html {
	  position: relative;
	  min-height: 100%;
	}
	body {
	  /* Margin bottom by footer height */
	  margin-bottom: 100px;
	}
	.footer {
	  position: absolute;
	  bottom: 0;
	  width: 100%;
	  /* Set the fixed height of the footer here */
	  height: 100px;
	}
	</style>
        @stack('headscripts')
    </head>
    <body class="align">
    <div class="" style="margin:15px;">
        @yield('content')
    </div>
	<footer class="footer">
            <div class="col-md-12">
            <div class="row">
                    <div class="col-sm-2 col-sm-offset-3 text-center">
                         <span class="">
                             <img src="{{secure_asset('css/icfoss.png')}}"  style="height:80px;"alt="">
                          </span>
                    </div>
                    <div class="col-sm-2 text-center">
                        <span class="">
                         <img src="{{secure_asset('css/It-mission.png')}}"  style="height:80px;"alt="">
                         </span>
                    </div>
                    <div class="col-sm-2 text-center">
                        <span class="">
                            <img src="{{secure_asset('css/ksitl.png')}}"  style="height:80px;"alt="">
                         </span>
                    </div>

                </div>
            </div>
	</footer>
        @stack('bodyscripts')
    </body>
</html>
