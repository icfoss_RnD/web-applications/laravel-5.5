@extends('layouts.error')
@push('headscripts')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" />
<style>
.footer {
height: 50px;
}
</style>
<script>
var APP_URL = '{{URL::to("/")}}';
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/dataTables.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
@endpush
@section('content')
<div class="container">
    <h3 style="text-decoration: none !important;color:black !important" class="text-center">
    <a class="btn btn-primary" href="{{URL::to('/view_stats')}}">View Statistics</a>
    <!-- <a class="btn btn-success" href="{{URL::to('/view_map')}}">View Map</a> -->
    </h3>
    <div class="row">
    <div class="col-md-12">

    <form autocomplete="off" class="form-horizontal">
    <div class="form-section">
        <div class="form-group">
         <label for="district" class="col-md-4 control-label padding-left">Select your district</label>
            <div class="col-md-4 inputGroupContainer">
                <div class="">
                <select class="form-control" id="district" name="district">
                    <option value="" selected="selected">Select Your District</option>
                    <option value="THIRUVANANTHAPURAM" >THIRUVANANTHAPURAM</option>
                    <option value="KOLLAM" >KOLLAM</option>
                    <option value="ALAPPUZHA" >ALAPPUZHA</option>
                    <option value="PATHANAMTHITTA" >PATHANAMTHITTA</option>
                    <option value="KOTTAYAM" >KOTTAYAM</option>
                    <option value="IDUKKI" >IDUKKI</option>
                    <option value="ERNAKULAM" >ERNAKULAM</option>
                    <option value="THRISSUR" >THRISSUR</option>
                    <option value="PALAKKAD" >PALAKKAD</option>
                    <option value="MALAPPURAM" >MALAPPURAM</option>
                    <option value="KOZHIKODE" >KOZHIKODE</option>
                    <option value="WAYANAD" >WAYANAD</option>
                    <option value="KANNUR" >KANNUR</option>
                    <option value="KASARAGOD" >KASARAGOD</option>
                </select>
                </div>
            </div>
        </div>
    </div>
    </form>

    </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
        <table id="example" class="table table-hover table-bordered display row-border hover order-column" style="width:100%">
            <thead>
                <tr>
                    <th>Department</th>
                    <th>Office</th>
                    <th>Position (Lat, Long, Accuracy)</th>
                    <th>Place</th>
                    <th>City</th>
                    <th>District</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
        </div>
    </div>
</div>
@endsection
@push('bodyscripts')
<script>
function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : (sParameterName[1]);
        }
    }
}
var dept = getUrlParameter('dept');
getDataDistrictWise(dept);
$('#district').on('change',function(e){
    $('#example').DataTable().destroy();
    $('#example tbody').html('');
    var district = $('#district').val();
    getDataDistrictWise(dept,district);
});
function getDataDistrictWise(dept='',district=''){
    if(dept!='' || district!=''){
        $.ajax({
        type: 'GET',
        dataType: 'json',
        url: APP_URL+'/map_data?bbox=1,1,100,100&district='+district+'&dept='+dept,
        success: function(result){
            var data = result.data;
            $.each(data, function(index, value){
                var html = '<tr>';
                html += '<td>'+value.department_name+'</td>';
                html += '<td>'+value.office_name+'</td>';
                html += '<td>'+value.gps_lat+', '+value.gps_lng+', '+value.gps_accy+'</td>';
                html += '<td>'+value.place+'</td>';
                html += '<td>'+value.city+'</td>';
                html += '<td>'+value.district+'</td>';
                html += '</tr>';
                $('#example tbody').append(html);
            });
            $('#example').DataTable({
                dom:  "<'row'<'col-sm-2'l><'col-sm-3'B><'col-sm-6'f>>" +
                      "<'row'<'col-sm-12'tr>>" +
                      "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Export to Excel',
                        title: district+' - MapMyOffice data'
                    },
                ]
            });
        }
        });
    }
}
</script>
@endpush