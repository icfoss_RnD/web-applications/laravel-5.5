@extends('layouts.master')
@section('content')
<div class="container">
<div class="row mt-10px">
  <div class="col-md-12 text-center text-info">
    <h4><a class="nav-link" href="{{ URL::to('/video') }}">Click here for instructional video</a></h4>
    <h4><a class="nav-link" href="{{secure_asset('pdf/circular.pdf')}}">Click here for Circular</a></h4>
    <h5>For further enquiries, contact <a class="nav-link" href="tel:9745004213">9745004213</a></h5>
  </div>
   <div class="col-md-offset-1 col-md-10 hidden-xs well">
   <h2 class="text-center text-warning">Please use your smartphone to access the website</h2>
   </div>
   <div class="col-md-offset-1 col-md-10 visible-xs">
   <form autocomplete="off" method="POST" accept-charset="UTF-8" id="login_form" class="well form-horizontal" action="{{ URL('/otp_verify') }}">
      {{csrf_field()}}
      <span class="site_logo visible-xs">
        <img src="{{secure_asset('images/app_logo.png')}}" class="w-35per" alt="Map My Office">
      </span>
      <span class="site_logo hidden-xs">
        <img src="{{secure_asset('images/app_logo.png')}}" class="w-15per" alt="Map My Office">
      </span>
      <div class="panel-body">
         <div class="form-section">
            <div class="form-group">
               <!-- <label for="user_name" class="col-md-4 control-label padding-left">Phone Number</label> -->
                  <div class="col-md-offset-4 col-md-4 inputGroupContainer">
                     <div class="">
                        <input maxlength="10" placeholder="Mobile Number" class="form-control number" name="mobile_number" type="number" pattern="[0-9]*" inputmode="numeric" id="mobile_number" />
                     </div>
                  </div>
            </div>
         </div>
         <div class="form-section">
            <div class="form-group">
               <!-- <label for="user_name" class="col-md-4 control-label padding-left">Phone Number</label> -->
                  <div class="col-md-offset-4 col-md-4 inputGroupContainer">
                     <input maxlength="6" placeholder="Type OTP here" class="form-control number" name="otp" type="number" pattern="[0-9]*" inputmode="numeric" id="otp" disabled="disabled" />
                  </div>
            </div>
         </div>
         <div class="form-group" id="login_reg">
            <div class="col-md-12">
               <button id="otp_login" type="submit" class="btn btn-primary">Send OTP</button>
               <button id="otp_retry" type="button" class="btn btn-warning">Retry</button>
            </div>
         </div>
      </div>

      <!-- Button -->
      <div class="form-group text-center">
         <label class="col-md-12">OR <br/><br/> Login using</label>
         <div class="col-md-12">
               <a class="col-sm-2 btn-xs-block btn btn-default float-none" href="{{ url('/login/gitlab') }}">
                  <i class="fa fa-gitlab"></i> Gitlab
               </a>
               <a class="col-sm-2 btn-xs-block btn btn-default float-none" href="{{ url('/login/github') }}">
                  <i class="fa fa-github"></i> Github
               </a>
               <a class="col-sm-2 btn-xs-block btn btn-default float-none" href="{{ url('/login/google') }}">
                  <i class="fa fa-google"></i> Google
               </a>
               <a class="col-sm-2 btn-xs-block btn btn-default float-none" href="{{ url('/login/facebook') }}">
                  <i class="fa fa-facebook"></i> Facebook
               </a>
         </div>
      </form>
      </div>
   </div>
</div>
</div>

<div id="messageModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <p id="modal_message">Thank you</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div id="loadingModal" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body text-center noselect">
   	<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><br>Loading..
      </div>
    </div>
  </div>
</div>

@endsection
@push('bodyscripts')
<script type='text/javascript'>
$(document).ready(function() {
$('#login_form')[0].reset();
$('#login_form').bootstrapValidator({
    // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        mobile_number: {
            validators: {
               stringLength: {
                  min: 10,
                  max: 10,
                  message: 'Please enter a valid Mobile Number'
               },
               integer: {
                  message: 'Please enter a valid Mobile Number'
               },
               notEmpty: {
                  message: 'Please enter a valid Mobile Number'
               }
            }
        },
        otp: {
            validators: {
               stringLength: {
                  min: 6,
                  max: 6,
                  message: 'Please enter OTP received on your mobile phone'
               },
               integer: {
               message: 'Please enter OTP received on your mobile phone'
               },
               notEmpty: {
                  message: 'Please enter OTP received on your mobile phone'
               }
            }
        },
    }
}).on('error.validator.bv', function(e, data) {
   data.element
      .data('bv.messages')
      // Hide all the messages
      .find('.help-block[data-bv-for="' + data.field + '"]').hide()
      // Show only message associated with current validator
      .filter('[data-bv-validator="' + data.validator + '"]').show();
}).on('success.form.bv', function(e) {
   $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
   $('#login_form').data('bootstrapValidator').resetForm();
   // Prevent form submission
   e.preventDefault();
   // Get the form instance
   var $form = $(e.target);
   // Get the BootstrapValidator instance
   var bv = $form.data('bootstrapValidator');
   // Use Ajax to submit form data
   $.post($form.attr('action'), $form.serialize(), function(result) {
      //OTP send
      if(result.hasOwnProperty('message_id')){
         $('#otp_login').html('Verify OTP');
         $('#otp').removeAttr('disabled');
         $('#otp').focus();
         $('#mobile_number').attr('disabled','disabled');
      }
      //success login
      if(result.hasOwnProperty('redirect')){
         window.location.href = result.redirect;
      }
      //invalid OTP
      if(result.hasOwnProperty('message')){
         $('#modal_message').html('<span class="text-danger text-center">'+result.message+'</span>');
         $('#messageModal').modal('show');
      }
      else{
      }
   }, 'json');
});
});

$(document).ajaxStart(function(){
  $("#loadingModal").modal("show");
});
$(document).ajaxComplete(function(){
  $("#loadingModal").modal("hide");
});

$('#otp_retry').on('click',function(e){
   $('#otp_login').html('Send OTP');
   $('#mobile_number').removeAttr('disabled');
   $('#otp').val('');
   $('#otp').attr('disabled','disabled');
});

$('.number').on('keyup keypress blur change', function(ev){
   return ev.charCode >= 48 && ev.charCode <= 57;
});


// Disable scroll when focused on a number input.
$('form').on('focus', 'input[type=number]', function(e) {
   $(this).on('wheel', function(e) {
      e.preventDefault();
   });
});

// Restore scroll on number inputs.
$('form').on('blur', 'input[type=number]', function(e) {
   $(this).off('wheel');
});

// Disable up and down keys.
$('form').on('keydown', 'input[type=number]', function(e) {
   if ( e.which == 38 || e.which == 40 )
      e.preventDefault();
});
</script>
@endpush
