@extends('layouts.master')
@push('headscripts')
<style>
  .footer {
    height: 4px;
  }
</style>  
@endpush
@section('content')
<div class="container">
<div class="row">
<div class="col-md-4 col-md-offset-4 text-center">
  <video class="img-responsive center" src="{{secure_asset('videos/mapmyoffice.mp4')}}" controls></video>
</div>
</div>
</div>
@endsection
@push('bodyscripts')
<script>
</script>
@endpush
