<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMapDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('map_data', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->float('gps_lat', 10, 0);
			$table->float('gps_lng', 10, 0);
			$table->float('gps_accy', 10, 0);
			$table->string('name', 30);
			$table->string('created_by', 30);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('map_data');
	}

}
