<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOfficenamesTable extends Migration {

	public function up()
	{
		Schema::create('Officenames', function(Blueprint $table) {
			$table->increments('id');
			$table->string('office_name', 191);
			$table->integer('department_id')->unsigned();
			$table->string('Department', 191);
			$table->string('ddocd', 191);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('Officenames');
	}
}