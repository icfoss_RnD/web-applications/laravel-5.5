<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('Officenames', function(Blueprint $table) {
			$table->foreign('department_id')->references('id')->on('Departments')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	public function down()
	{
		Schema::table('Officenames', function(Blueprint $table) {
			$table->dropForeign('Officenames_department_id_foreign');
		});
	}
}