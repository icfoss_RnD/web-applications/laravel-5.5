<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',  function () {
    if(Session::has('name'))
     return redirect('/list_view');
    return view('login');
});

Route::resource('/map', 'MapController');

Route::get('/logout', function() {
    Session::forget('name');
    Session::forget('mobile');
    Session::forget('otp');
    if(!Session::has('name'))
     return redirect('/');
});



Auth::routes();

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/privacy', function () {
    return view('privacy');
});

Route::post('/app_data', 'MapController@receiveAppData');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/map_data','MapController@getMapData');

Route::post('otp_verify', 'Auth\LoginController@otpVerify');

Route::get('/video', function (){
  return view('video');
});

Route::get('/view_table', function(){
    return view('view_table');
});

Route::get('/view_map', function(){
    return view('view_map');
});

Route::get('/view_stats', function(){
    return view('view_stats');
});

Route::get('/districtwise_count','MapController@getDistrictwiseCount');
Route::get('/deptwise_count','MapController@getDeptwiseCount');
Route::get('/check_login','MapController@checkLogin');
Route::get('/list_view','MapController@listView');