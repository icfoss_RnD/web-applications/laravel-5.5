<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Session;
use DB;
use App\User;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use URL;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request){
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];
       
        if (Auth::attempt($credentials)) {
            Session::put('name',Auth::user()->name);
            return redirect('/map');
        }else{
	    return redirect('/');
//            return $credentials;
        }
    }


    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        
        $user = Socialite::driver($provider)->user();

        // OAuth Two Providers
        $token = $user->token;
        $refreshToken = $user->refreshToken; // not always provided
        $expiresIn = $user->expiresIn;

        // All Providers
        // $user->getId();
        // $user->getNickname();
        // $user->getName();
        // $user->getEmail();
        // $user->getAvatar();
        
        if($token!=null){
            $form_data = [
                'name' => $user->getName(),
                'email' => ($user->getEmail())?$user->getEmail():'',
                'provider' => $provider,
            ];
            $insert_flag = DB::table('social_login_details')
            ->updateOrInsert($form_data,$form_data);
                Session::put('name',$user->getName());
                return redirect('/list_view');
        }
        else{
            abort(403);
        }
    }

    public function otpVerify(Request $request){

        $mobile_number = $request->mobile_number;
        if($mobile_number!=null){
            $six_digit_otp = mt_rand(100000, 999999);
            Session::put('mobile',$mobile_number);
            Session::put('otp',$six_digit_otp);
            $sms_base_url = "http://api.esms.kerala.gov.in/fastclient/SMSclient.php";
            $sms_content = "OTP: ".$six_digit_otp;
            $sms_request_url = "$sms_base_url?username=ksdimapping&password=ksdi1234&senderid=KLMGov&numbers=$mobile_number&message=$sms_content";
            $client = new Client();
            $response = $client->request('GET',$sms_request_url);
            $msg_id = $response->getBody()->read(1024);
            return json_encode(['status'=>true,'message_id'=>$msg_id]);
        }

        $otp = $request->otp;
        if($otp!=null){
            $session_otp = Session::get('otp');
            if($otp == $session_otp){
                $mobile = Session::get('mobile');
                Session::put('name',$mobile);

                $form_data = [
                    'name' => $mobile,
                    'email' => '',
                    'provider' => 'OTP',
                ];
                $insert_flag = DB::table('social_login_details')
                ->updateOrInsert($form_data,$form_data);
                    
                return json_encode(['status'=>true,'redirect'=>URL::to('/list_view')]);
            }
            else{
                return json_encode(['status'=>false,'message'=>'Invalid OTP!']);
            }
        }

    }
}
