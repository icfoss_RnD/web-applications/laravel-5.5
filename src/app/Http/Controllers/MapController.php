<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
use URL;
use Illuminate\Support\Facades\Log;
class MapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(!Session::has('name'))
            return redirect('/');
        $user = Session::get('name');
        $departments = DB::table('department_master')->select('id', 'department_name')->orderBy('department_name')->get();
        return view('map',compact('user','departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(!Session::has('name'))
         return redirect('/');
        $form_data = [
            'gps_lat' => $request->gps_lat,
            'gps_lng' => $request->gps_lng,
            'gps_accy' => $request->gps_accy,
            'created_by' => Session::get('name'),
            'dept_id' => $request->dept_name,
            'office_id' => $request->office_name,
            'office_name' => $request->custom_office_name,
            'build_num' => $request->buildno,
            'street_name' => $request->streetname,
    	    'place' => $request->place,
            'post_code' => $request->post,
            'city' => $request->city,
            'district' => $request->district,
            'working_hours' => $request->working_hours,
            'phone_number' => $request->phonenum,
            'website' => $request->website,
            'localbody_type' => $request->lb_type,
            'office_mail' => $request->office_mail,
            'mobile_number' => $request->mobilenum,
            'extra_info' => $request->extra_info,
            'remarks' => $request->remarks,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ];

        $insert_flag = DB::table('map_data')->insert($form_data);
        Log::info('Map_data inserted: '.json_encode($form_data));
        return json_encode(['status'=>true,'redirect'=>URL::to('/list_view')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if(!Session::has('name'))
         return redirect('/');

        if($id == "data" && request()->ajax()){
            
            $map_data = DB::table('map_data')
            ->join('department_master','department_master.id','map_data.dept_id')
            // ->leftJoin('office_master','office_master.id','map_data.office_id')
            ->select('gps_lat','gps_lng','gps_accy','department_name','office_name','place','city','district','working_hours')
            ->get()->toArray();
            $response = [
                'data' => $map_data,
            ];
            return json_encode($response);
        }else{
            if(!Session::has('name'))
                return redirect('/');
            $user = Session::get('name');
            $officess=DB::table('map_data')->where('id',$id)->where('created_by',$user)->get();
            if(count($officess)==0)
                return redirect('/list_view');
            $departments = DB::table('department_master')->select('id', 'department_name')->orderBy('department_name')->get();
            return view('map',compact('user','departments','officess'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $form_data = [
            'created_by' => Session::get('name'),
            'office_name' => $request->custom_office_name,
            'build_num' => $request->buildno,
            'street_name' => $request->streetname,
    	    'place' => $request->place,
            'post_code' => $request->post,
            'city' => $request->city,
            'district' => $request->district,
            'working_hours' => $request->working_hours,
            'phone_number' => $request->phonenum,
            'website' => $request->website,
            'localbody_type' => $request->lb_type,
            'office_mail' => $request->office_mail,
            'mobile_number' => $request->mobilenum,
            'extra_info' => $request->extra_info,
            'remarks' => $request->remarks,
            'updated_at' => date("Y-m-d H:i:s"),
        ];
        $update_flag = DB::table('map_data')->where('id',$id)->update($form_data);
        Log::info('Map_data updated: '.json_encode($form_data));
        return json_encode(['status'=>true,'redirect'=>URL::to('/list_view')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete_flag = DB::table('map_data')->where('id',$id)->delete();
        return json_encode($delete_flag);
    }

    public function receiveAppData(Request $request){
        $xml_file_array = json_decode($request->xmlfile,true);
        foreach($xml_file_array as $xml){
                $app_data = [
                    "xml" => $xml,
                ];
            $auth_hash = $request->auth_hash;
            $check_hash = md5("Sal@mR0ckyBha!"); //APP_KEY
            if($auth_hash == $check_hash){
                    $flag = DB::table('app_data')->insert($app_data);
		    $response = [
        		'status' => 'true',
        		'message' => 'Thank you',
                ];
            }
	    else {
		    $response = [
       			'status' => 'false',
       			'message' => 'Error occured in saving',
                ];
	    }
        }
        return json_encode($response);
    }

    public function getMapData(Request $request){
        $bbox = explode(",",$request->bbox);
        if(count($bbox)<4){
            $response = [
                'status' => 'false',
                'data' => 'send bbox',
            ];
            return json_encode($response);
        }
        $dept=$request->dept;
        $district=$request->district;

        $map_data = DB::table('map_data')
            ->join('department_master','department_master.id','map_data.dept_id')
            // ->leftJoin('office_master','office_master.id','map_data.office_id')
            ->whereBetween('gps_lng',[$bbox[0],$bbox[2]])
            ->whereBetween('gps_lat',[$bbox[1],$bbox[3]])
            ->where(function($query) use ($dept,$district){
                if($dept){
                    $query->where('department_name',$dept);
                }
                if($district){
                    $query->where('district',$district);
                }
            })
            ->select('gps_lat','gps_lng','gps_accy','department_name','office_name','place','city','district','working_hours')
            ->get()->toArray();
        $response = [
            'status' => 'true',
            'data' => $map_data,
        ];
        return json_encode($response);
    }

    public function getDistrictwiseCount(Request $request){
        $count_obj = DB::select('SELECT count(office_name) as count FROM `map_data` right JOIN district_master ON map_data.district=district_master.name WHERE dept_id <> 111 GROUP BY district,district_master.id ORDER BY district_master.id');
        $result = array();
        foreach($count_obj as $value)
            array_push($result,$value->count);
        return $result;
    }

    public function getDeptwiseCount(Request $request){
        $count_obj = DB::select('SELECT department_master.department_name,count(office_name) as count FROM `map_data` right JOIN department_master ON map_data.dept_id=department_master.id WHERE 1 GROUP BY map_data.dept_id,department_master.id ORDER BY department_master.department_name');
        return $count_obj;
    }

    public function checkLogin(){
        if(!Session::has('name'))
            return redirect('/');
        $user = Session::get('name');
        $departments = DB::table('department_master')->select('id', 'department_name')->orderBy('department_name')->get();
        $data_count=DB::table('map_data')->where('created_by',$user)->count();
        if($data_count>0){
            return redirect('/list_view');
        }
    }

    public function listView(){
        if(!Session::has('name'))
            return redirect('/');
        $user = Session::get('name');
        $offices=DB::table('map_data')->where('created_by',$user)->get();
        // return $offices;
        $departments = DB::table('department_master')->select('id', 'department_name')->orderBy('department_name')->get();
        if(count($offices)==0){
            return redirect('/map');
        }
        return view('list_view',compact('user','departments','offices'));
    }
}
