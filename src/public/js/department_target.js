var target_jsonarr = [
  {
    "department": "Administration of Justice-Judiciary",
    "target": 601
  },
  {
    "department": "Administrative Reforms Commission",
    "target": 2
  },
  {
    "department": "Advocate General",
    "target": 4
  },
  {
    "department": "Agriculture",
    "target": 1373
  },
  {
    "department": "Akshaya",
    "target": 2717
  },
  {
    "department": "Animal Husbandry",
    "target": 1367
  },
  {
    "department": "Archaeology",
    "target": 12
  },
  {
    "department": "Archives",
    "target": 5
  },
  {
    "department": "Autonomous Bodies",
    "target": 47
  },
  {
    "department": "Ayurveda Medical Education",
    "target": 15
  },
  {
    "department": "Backward Communities Development",
    "target": 3
  },
  {
    "department": "Chemical Examiners Laboratory",
    "target": 4
  },
  {
    "department": "Civil Supplies",
    "target": 115
  },
  {
    "department": "Co-Operation",
    "target": 193
  },
  {
    "department": "Coir Development",
    "target": 55
  },
  {
    "department": "Commercial Taxes",
    "target": "NA"
  },
  {
    "department": "Commissioner for Entrance Examination",
    "target": 2
  },
  {
    "department": "Culture",
    "target": 1
  },
  {
    "department": "Dairy Development",
    "target": 208
  },
  {
    "department": "Deputation to Central Service (AIS)",
    "target": 10
  },
  {
    "department": "Deputation to other organisations",
    "target": 1
  },
  {
    "department": "Drugs Control Department",
    "target": 23
  },
  {
    "department": "Economics & Statistics",
    "target": 83
  },
  {
    "department": "Education",
    "target": 1
  },
  {
    "department": "Education - Technical",
    "target": 160
  },
  {
    "department": "Education (Collegiate)",
    "target": 285
  },
  {
    "department": "Education (General)",
    "target": 12308
  },
  {
    "department": "Education (Higher Secondary)",
    "target": 1653
  },
  {
    "department": "Education (Law Colleges)",
    "target": 8
  },
  {
    "department": "Education (Sports School Division)",
    "target": 1
  },
  {
    "department": "Education (Vocational Higher Secondary)",
    "target": 408
  },
  {
    "department": "Election",
    "target": 3
  },
  {
    "department": "Electrical Inspectorate",
    "target": 17
  },
  {
    "department": "Electronics and Information Technology",
    "target": "NA"
  },
  {
    "department": "Enquiry Commissioner and Special Judge",
    "target": 10
  },
  {
    "department": "Environment Department",
    "target": 2
  },
  {
    "department": "Excise",
    "target": 343
  },
  {
    "department": "Factories & Boilers",
    "target": 52
  },
  {
    "department": "Finance Department",
    "target": 15
  },
  {
    "department": "Fire Force",
    "target": 140
  },
  {
    "department": "Fisheries",
    "target": 127
  },
  {
    "department": "Food Safety Department",
    "target": 88
  },
  {
    "department": "FOREST AND WILDLIFE",
    "target": 342
  },
  {
    "department": "General Administration Department",
    "target": 30
  },
  {
    "department": "Ground Water",
    "target": 22
  },
  {
    "department": "Handloom",
    "target": 2
  },
  {
    "department": "Harbour Engineering",
    "target": 36
  },
  {
    "department": "Health Services",
    "target": 1356
  },
  {
    "department": "HINDU RELIGIOUS AND CHARITABLE ENDOWMENT",
    "target": 8
  },
  {
    "department": "Homoeopathic Medical Colleges",
    "target": 9
  },
  {
    "department": "Homoeopathy",
    "target": 714
  },
  {
    "department": "Housing",
    "target": 26
  },
  {
    "department": "Hydrographic Survey Wing",
    "target": 9
  },
  {
    "department": "IMG",
    "target": 1
  },
  {
    "department": "Indian Systems of Medicine",
    "target": 969
  },
  {
    "department": "Industrial Training",
    "target": 108
  },
  {
    "department": "Industrial Tribunals",
    "target": 8
  },
  {
    "department": "Industries & Commerce",
    "target": 80
  },
  {
    "department": "Information & Public Relations",
    "target": 24
  },
  {
    "department": "Insurance Medical Services",
    "target": 166
  },
  {
    "department": "Jails",
    "target": 61
  },
  {
    "department": "Judicial Services",
    "target": 1
  },
  {
    "department": "Kerala State Audit Department",
    "target": 59
  },
  {
    "department": "KIRTADS",
    "target": 2
  },
  {
    "department": "Labour",
    "target": 148
  },
  {
    "department": "Labour Courts",
    "target": 4
  },
  {
    "department": "Land Use Board",
    "target": 3
  },
  {
    "department": "Legal Metrology",
    "target": 103
  },
  {
    "department": "Legislature Secretariat",
    "target": 3
  },
  {
    "department": "Local Self-government Offices",
    "target": "NA"
  },
  {
    "department": "Lotteries",
    "target": 39
  },
  {
    "department": "LSGD Engineering Wing",
    "target": 967
  },
  {
    "department": "Medical Education",
    "target": 45
  },
  {
    "department": "Mining & Geology",
    "target": 18
  },
  {
    "department": "Minority Welfare",
    "target": 3
  },
  {
    "department": "Motor Vehicles Department",
    "target": 85
  },
  {
    "department": "Muncipal Administration",
    "target": 90
  },
  {
    "department": "Museums and Zoos",
    "target": 4
  },
  {
    "department": "National Cadet Corps",
    "target": 55
  },
  {
    "department": "National Employment Service",
    "target": 98
  },
  {
    "department": "National Savings",
    "target": 16
  },
  {
    "department": "NORKA",
    "target": 'NA'
  },
  {
    "department": "PANCHAYATS",
    "target": 1059
  },
  {
    "department": "Police",
    "target": 985
  },
  {
    "department": "Port",
    "target": 18
  },
  {
    "department": "Power Department",
    "target": "NA"
  },
  {
    "department": "Printing",
    "target": 24
  },
  {
    "department": "Public Library",
    "target": 2
  },
  {
    "department": "Public Service Commission",
    "target": 19
  },
  {
    "department": "Public Works Department",
    "target": 3
  },
  {
    "department": "PW-Buildings",
    "target": 314
  },
  {
    "department": "PW-Irrigation",
    "target": 764
  },
  {
    "department": "PW-NH",
    "target": 70
  },
  {
    "department": "PW-Roads and Bridges",
    "target": 337
  },
  {
    "department": "Rajbhavan",
    "target": 4
  },
  {
    "department": "Registration",
    "target": 360
  },
  {
    "department": "Revenue",
    "target": 1786
  },
  {
    "department": "Revenue- Land Board",
    "target": 3
  },
  {
    "department": "Rural Development",
    "target": 238
  },
  {
    "department": "Sainik Welfare",
    "target": 16
  },
  {
    "department": "Scheduled Caste Development",
    "target": 338
  },
  {
    "department": "Scheduled Tribes Development",
    "target": 173
  },
  {
    "department": "Secretariat",
    "target": 1
  },
  {
    "department": "Secretariat-Law",
    "target": 46
  },
  {
    "department": "Social Justice Department",
    "target": 88
  },
  {
    "department": "Soil Survey and Soil Conservation",
    "target": 76
  },
  {
    "department": "Sports & Youth Affairs",
    "target": 3
  },
  {
    "department": "State Goods and Services Tax Department Kerala",
    "target": 237
  },
  {
    "department": "State Insurance",
    "target": 16
  },
  {
    "department": "State Planning Board",
    "target": 16
  },
  {
    "department": "State Water Transport",
    "target": 16
  },
  {
    "department": "Stationery Department",
    "target": 17
  },
  {
    "department": "Survey and Land Records",
    "target": 68
  },
  {
    "department": "Tourism",
    "target": 52
  },
  {
    "department": "Town Planning",
    "target": 17
  },
  {
    "department": "Treasuries",
    "target": 236
  },
  {
    "department": "University Appellate Tribunal",
    "target": 2
  },
  {
    "department": "Vigilance",
    "target": 35
  },
  {
    "department": "Vigilance Tribunal",
    "target": 4
  },
  {
    "department": "Women & Child Development",
    "target": 325
  },
  {
    "department": "Womens Commission",
    "target": 2
  }
];
