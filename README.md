# Map My Office

Steps for deployment

- Clone the repository branch gov_buildings
- Import the sql dump to db
- Install docker, docker-compose
- Run **docker-compose up -d** from repo
- Run **docker exec -ti web composer update**
- Update ports of each container as required
- Setup nginx with letsencrypt and proxy for web [see this repo](https://gitlab.com/aaj013/nginx-letsencrypt.git)
